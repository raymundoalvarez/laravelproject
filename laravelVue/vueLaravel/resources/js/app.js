
require('./bootstrap');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('component-register', require('./components/registerComponent.vue').default);
Vue.component('all-component', require('./components/allComponent.vue').default);


const app = new Vue({
    el: '#app',
});



