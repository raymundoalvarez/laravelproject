@extends('basic')

@section('pageData')
    @parent

    <div class="container">
        <h4>Registros en base de datos mongo: </h4>


    
            <div class="table-wrapper">
                <table class="fl-table">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Información</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($registerUser as $dataUsers)
                    <tr>
                        <td>{{$dataUsers -> nombre}}</td>
                        <td>{{$dataUsers -> informacion}}</td>
                    </tr>
                    @endforeach
                    <tbody>
                </table>
            </div>



        <!-- <ul>
            @foreach($registerUser as $dataUsers)
            <li>{{$dataUsers -> nombre}} - {{$dataUsers -> informacion}}</li>
            @endforeach
        </ul> -->


    <div class="containerform">
         <form method="post" action="{{url('savedata')}}">
            <input type="text" name="nombre" placeholder="Nombre"><br>
            <input type="text" name="informacion" placeholder="Informaciòn a guardar"><br>
            <input type="submit" value="Registrar">
            <!-- <input type="hidden" name="_token" value="{{ csrf_token()}}"> -->
            {{csrf_field()}}

        </form>
    </div>

    </div>


@stop