@extends('basic')

@section('pageData')
    @parent

    <div class="container">
        <all-component :prop-message = "{{$dataUser}}"></all-component>
    </div>

@stop