<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="/css/basic.css" media="screen">
        <link href="{{ asset('css/basic.css') }}" rel="stylesheet" type="text/css" >
        <title>Laravel and Vue</title>
    </head>

    <body>
            <header class="header">
                <a href="" class="logo">Ejecicios </a>
                <input class="menu-btn" type="checkbox" id="menu-btn" />
                <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
                <ul class="menu">
                    <li><a href="{{route('mongo')}}">Mongo</a></li>
                    <li><a href="{{route('vue')}}">Vue</a></li>
                    <li><a href="{{route('vuemongo')}}">Vue+laravel+Mongo</a></li>
                </ul>
            </header>

            <div id="app">
            @section('pageData')
            @show
            </div>

        

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
