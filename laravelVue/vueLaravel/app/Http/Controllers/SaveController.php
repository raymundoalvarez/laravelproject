<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Register;

class SaveController extends Controller
{

    public function savedata(){

        $info = new Register();

        $info->nombre = request('nombre');
        $info->informacion = request('informacion');
        $info->save();
 
        return redirect('/mongo');

    }
  
}
