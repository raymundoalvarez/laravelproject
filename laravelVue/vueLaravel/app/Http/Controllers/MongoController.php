<?php

namespace App\Http\Controllers;

use App\Register;
use Illuminate\Http\Request;

class MongoController extends Controller
{

    public function getdata(){
        $registerUser = Register::all();
        return view('mongo',compact('registerUser'));
    }
    
}
