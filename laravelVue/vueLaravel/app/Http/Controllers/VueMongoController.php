<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Register;

class VueMongoController extends Controller
{
    public function data(){
        $dataUser = Register::all();
        return view('vuemongo',compact('dataUser'));
    }
}
