<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable as Authenticablerait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Eloquent implements Authenticatable
{

    use Authenticablerait;
    use Notifiable;

    
    protected $connection = 'mongodb';
    protected $collection = 'UserInformation';



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'informacion',
    ];

}
