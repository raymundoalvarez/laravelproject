<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Register extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'UserInformation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'informacion',
    ];

}
